<?php
/**
 * Created by: hugh
 * Date: 12/3/16
 * Time: 3:55 PM
 */

/**
 * Implements hook_feeds_plugins().
 */
function bc_feeds_plugins() {
  $info = array();
  $info['FeedsEntityProcessorBc'] = array(
    'name' => 'Beancount entity processor',
    'description' => 'Process Beancount data.',
    'help' => 'Create and update Beancount entities from parsed content.',
    'plugin_key' => 'FeedsEntityProcessorBc',
    'handler' => array(
      'parent' => 'FeedsEntityProcessor',
      // A plugin needs to derive either directly or indirectly from FeedsFetcher, FeedsParser or FeedsProcessor.
      'class' => 'FeedsEntityProcessorBc',
      'file' => 'FeedsEntityProcessorBc.inc',
      'path' => drupal_get_path('module', 'bc') . '/src',
    ),
    'type' => 'bc'
  );
  return $info;
}

/**
 * Implements hook_feeds_processor_targets_alter().
 * */
function bc_feeds_processor_targets_alter(array &$targets, $entity_type, $bundle_name) {
  if ($entity_type == 'bc') {
    // Add a unique callback for each target field.
    foreach ($targets as &$target) {
      if (isset($target['name'])) {
        if ((($target['name'] === 'ExtID') || stristr($target['name'], 'unique'))) {
          $target['unique_callbacks'][] = 'bc_feeds_unique_callback';
          $target['bundle_name'] = $bundle_name;
          $target['optional_unique'] = TRUE;
        }
      }
    }
  }
}

/**
 * Feeds unique callback for ext-id.
 *
 * @param FeedsSource $source
 *   The Feed source.
 * @param string $entity_type
 *   Entity type for the entity to be processed.
 * @param string $bundle
 *   Bundle name for the entity to be processed.
 * @param string $target
 *   A string identifying the unique target on the entity.
 * @param array $values
 *   The unique values to be checked.
 *
 * @return int
 *   The existing entity id, or NULL if nothing is found.
 *
 * @see data_entity_feeds_processor_targets_alter().
 * @see FeedsProcessor::existingEntityId()
 */
function bc_feeds_unique_callback(FeedsSource $source, $entity_type, $bundle, $target, $values) {
  // Get the information about this entity.
  $entity_info = entity_get_info($entity_type);
  // Extract the entity ID key.
  $entity_id_key = $entity_info['entity keys']['id'];
  $value = array_pop($values);

  $result = db_select('bc', 'b')
    ->condition('b.' . $target, $value)
    ->fields('b', array($entity_id_key))
    ->execute();
  $returnval = $result->fetchField();
  return ($returnval);
}
