<?php
/**
 * @file
 * Hooks provided by this module.
 */


/*
 * Ctools API plugin for Beancount Entities
 * Plugin elements:
 * - 'name' - the name of this Beancount entity / bundle
 * - 'subtypes' - define subtypes for this Beancount type
 *     array( 'subtype1', 'subtype2'... )
 * - 'form' - callback function to override the default BC admin form entirely
 *     form function($form, &$form_state, $entity)
 * - 'form alter $bctype' - callback to alter the default BC admin form
 *     form alter function(&$form, $entity)
 */



/**
 * @addtogroup hooks
 * @{
 */

/**
 * Acts on bc being loaded from the database.
 *
 * This hook is invoked during $bc loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array $entities
 *   An array of $bc entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_bc_load(array $entities) {
  $result = db_query('SELECT pid, foo FROM {mytable} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
 * Responds when a $bc is inserted.
 *
 * This hook is invoked after the $bc is inserted into the database.
 *
 * @param Bc $bc
 *   The $bc that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_bc_insert(Bc $bc) {
  db_insert('mytable')
    ->fields(array(
      'id' => entity_id('bc', $bc),
      'extra' => print_r($bc, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a $bc being inserted or updated.
 *
 * This hook is invoked before the $bc is saved to the database.
 *
 * @param Bc $bc
 *   The $bc that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_bc_presave(Bc $bc) {
  $bc->name = 'foo';
}

/**
 * Responds to a $bc being updated.
 *
 * This hook is invoked after the $bc has been updated in the database.
 *
 * @param Bc $bc
 *   The $bc that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_bc_update(Bc $bc) {
  db_update('mytable')
    ->fields(array('extra' => print_r($bc, TRUE)))
    ->condition('id', entity_id('bc', $bc))
    ->execute();
}

/**
 * Responds to $bc deletion.
 *
 * This hook is invoked after the $bc has been removed from the database.
 *
 * @param Bc $bc
 *   The $bc that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_bc_delete(Bc $bc) {
  db_delete('mytable')
    ->condition('pid', entity_id('bc', $bc))
    ->execute();
}

/**
 * Act on a bc that is being assembled before rendering.
 *
 * @param $bc
 *   The bc entity.
 * @param $view_mode
 *   The view mode the bc is rendered in.
 * @param $langcode
 *   The language code used for rendering.
 *
 * The module may add elements to $bc->content prior to rendering. The
 * structure of $bc->content is a renderable array as expected by
 * drupal_render().
 *
 * @see hook_entity_prepare_view()
 * @see hook_entity_view()
 */
function hook_bc_view($bc, $view_mode, $langcode) {
  $bc->content['my_additional_field'] = array(
    '#markup' => $additional_field,
    '#weight' => 10,
    '#theme' => 'mymodule_my_additional_field',
  );
}

/**
 * Alter the results of entity_view() for bc_entities.
 *
 * @param $build
 *   A renderable array representing the bc content.
 *
 * This hook is called after the content has been assembled in a structured
 * array and may be used for doing processing which requires that the complete
 * bc content structure has been built.
 *
 * If the module wishes to act on the rendered HTML of the bc rather than
 * the structured content array, it may use this hook to add a #post_render
 * callback. Alternatively, it could also implement hook_preprocess_bc().
 * See drupal_render() and theme() documentation respectively for details.
 *
 * @see hook_entity_view_alter()
 */
function hook_bc_view_alter($build) {
  if ($build['#view_mode'] == 'full' && isset($build['an_additional_field'])) {
    // Change its weight.
    $build['an_additional_field']['#weight'] = -10;

    // Add a #post_render callback to act on the rendered HTML of the entity.
    $build['#post_render'][] = 'my_module_post_render';
  }
}

/**
 * Acts on bctype being loaded from the database.
 *
 * This hook is invoked during bctype loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array $entities
 *   An array of bctype entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_bctype_load(array $entities) {
  $result = db_query('SELECT pid, foo FROM {mytable} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
 * Responds when a bctype is inserted.
 *
 * This hook is invoked after the bctype is inserted into the database.
 *
 * @param BcType $bctype
 *   The bctype that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_bctype_insert(BcType $bctype) {
  db_insert('mytable')
    ->fields(array(
      'id' => entity_id('bctype', $bctype),
      'extra' => print_r($bctype, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a bctype being inserted or updated.
 *
 * This hook is invoked before the bctype is saved to the database.
 *
 * @param BcType $bctype
 *   The bctype that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_bctype_presave(BcType $bctype) {
  $bctype->name = 'foo';
}

/**
 * Responds to a bctype being updated.
 *
 * This hook is invoked after the bctype has been updated in the database.
 *
 * @param BcType $bctype
 *   The bctype that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_bctype_update(BcType $bctype) {
  db_update('mytable')
    ->fields(array('extra' => print_r($bctype, TRUE)))
    ->condition('id', entity_id('bctype', $bctype))
    ->execute();
}

/**
 * Responds to bctype deletion.
 *
 * This hook is invoked after the bctype has been removed from the database.
 *
 * @param BcType $bctype
 *   The bctype that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_bctype_delete(BcType $bctype) {
  db_delete('mytable')
    ->condition('pid', entity_id('bctype', $bctype))
    ->execute();
}

/**
 * Define default bctype configurations.
 *
 * @return
 *   An array of default bctype, keyed by machine names.
 *
 * @see hook_default_bctype_alter()
 */
function hook_default_bctype() {
  $defaults['main'] = entity_create('bctype', array(
    // …
  ));
  return $defaults;
}

/**
 * Alter default bctype configurations.
 *
 * @param array $defaults
 *   An array of default bctype, keyed by machine names.
 *
 * @see hook_default_bctype()
 */
function hook_default_bctype_alter(array &$defaults) {
  $defaults['main']->name = 'custom name';
}

