<?php

/**
 * Attempts to directly activate a field that was disabled due to its module
 * being disabled.
 *
 * The normal API function for updating fields, field_update_field(), will not
 * work on disabled fields. As a workaround, this function directly updates the
 * database, but it is up to the caller to clear the cache.
 *
 *  This is mirror of commerce.module equivalent
 *
 * @param $field_name
 *   The name of the field to activate.
 *
 * @return
 *   Boolean indicating whether or not the field was activated.
 */
function bc_activate_field($field_name) {
  // Set it to active via a query because field_update_field() does
  // not work on inactive fields.
  $updated = db_update('field_config')
    ->fields(array('active' => 1))
    ->condition('field_name', $field_name, '=')
    ->condition('deleted', 0, '=')
    ->execute();

  return !empty($updated) ? TRUE : FALSE;
}

/**
 * Enables and deletes fields of the specified type.
 *
 *  This is mirror of commerce.module equivalent
 *
 * @param $type
 *   The type of fields to enable and delete.
 */
function bc_delete_fields($type, $bundle = NULL) {
  $fields_to_delete = array( 'type' => $type );
  if( $bundle ) {
    $fields_to_delete['bundle'] = $bundle;
  }
  // Read the fields for any active or inactive field of the specified type.
  foreach (field_read_fields($fields_to_delete, array('include_inactive' => TRUE)) as $field_name => $field) {
    bc_delete_field($field_name);
  }
}

/**
 * Enables and deletes the specified field.
 *
 * The normal API function for deleting fields, field_delete_field(), will not
 * work on disabled fields. As a workaround, this function first activates the
 * fields of the specified type and then deletes them.
 *
 * This is mirror of commerce.module equivalent
 *
 * @param $field_name
 *   The name of the field to enable and delete.
 */
function bc_delete_field($field_name) {
  // In case the field is inactive, first activate it and clear the field cache.
  if (bc_activate_field($field_name)) {
    field_cache_clear();
  }

  // Delete the field.
  field_delete_field($field_name);
}

/**
 * Deletes any field instance attached to entities of the specified type,
 * regardless of whether or not the field is active.
 *
 * This is mirror of commerce.module equivalent
 *
 * @param $entity_type
 *   The type of entity whose fields should be deleted.
 * @param $bundle
 *   Optionally limit instance deletion to a specific bundle of the specified
 *   entity type.
 */
function bc_delete_instances($entity_type, $bundle = NULL) {
  // Prepare a parameters array to load the specified instances.
  $params = array(
    'entity_type' => $entity_type,
  );

  if (!empty($bundle)) {
    $params['bundle'] = $bundle;
    // Delete this bundle's field bundle settings.
    variable_del('field_bundle_settings_' . $entity_type . '__' . $bundle);
  }
  else {
    // Delete all field bundle settings for this entity type.
    db_delete('variable')
      ->condition('name', db_like('field_bundle_settings_' . $entity_type . '__') . '%', 'LIKE')
      ->execute();
  }

  // Read and delete the matching field instances.
  foreach (field_read_instances($params, array('include_inactive' => TRUE)) as $instance) {
    bc_delete_instance($instance);
  }
}

/**
 * Deletes the specified instance and handles field cleanup manually in case the
 * instance is of a disabled field.
 *
 * This is mirror of commerce.module equivalent
 *
 * @param $instance
 *   The field instance info array to be deleted.
 */
function bc_delete_instance($instance) {
  // First activate the instance's field if necessary.
  $field_name = $instance['field_name'];
  $activated = bc_activate_field($field_name);

  // Clear the field cache if we just activated the field.
  if ($activated) {
    field_cache_clear();
  }

  // Then delete the instance.
  field_delete_instance($instance, FALSE);

  // Now check to see if there are any other instances of the field left.
  $field = field_info_field($field_name);

  if (count($field['bundles']) == 0) {
    field_delete_field($field_name);
  }
  elseif ($activated) {
    // If there are remaining instances but the field was originally disabled,
    // disabled it again now.
    $field['active'] = 0;
    field_update_field($field);
  }
}

