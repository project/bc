<?php

$plugin = array(
  'name' => 'account',
  'form alter account' => array(
    // 'file' => 'filename', // defaults to this file
    // 'path' => 'filepath', // optional, will use plugin path if absent
    'function' => '_bc_form_alter_account',
  ),
  'subtypes' => array(
    'accounts payable',
    'accounts receivable',
    'bank',
    'cost of goods sold',
    'credit card',
    'equity',
    'expense',
    'fixed asset',
    'income',
    'long term liability',
    'non posting',
    'other asset',
    'other current asset',
    'other current liability',
    'other expense',
    'other income',
  ),
  'addmod external sync' => 'bc_qbxml_form_generate_addmod',
);

/**
 * Implements ctools 'form alter' callback
 * @param $form
 * @param $entity
 * @return mixed
 */
function _bc_form_alter_account(&$form, $entity) {
  $form['bc_value']['#access'] = FALSE;
  $form['bc_value_sub']['#access'] = FALSE;

  $data = isset($entity->data) ? json_decode($entity->data, TRUE) : array();
  foreach (array(
             'AccountNumber',
             'BankNumber',
             'OpenBalanceDate',
             /*'CashflowClass',*/
             'TaxLineId',
             'TaxLineName'
           ) as $fieldname) {
    $form['-data-' . $fieldname] = array(
      '#type' => 'textfield',
      '#title' => t(ucfirst(implode(' ', qbxml_camelterm_to_array($fieldname)))),
      '#default_value' => empty($data[$fieldname]) ? NULL : t($data[$fieldname]),
    );
  }
}