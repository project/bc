<?php

$plugin = array(
  'name' => 'party',
  'form alter party' => array(
    // 'file' => 'filename', // defaults to this file
    // 'path' => 'filepath', // optional, will use plugin path if absent
    'function' => '_bc_form_alter_party',
  ),
  'subtypes' => array(
    'customer', 
    'employee', 
    'other name', 
    'vendor',
  ),
  'sync to external' => 'quickbooks',
);

/**
 * Implements ctools 'form alter' callback
 * @param $form
 * @param $entity
 * @return mixed
 */
function _bc_form_alter_party(&$form, $entity) {
  $form['bc_value']['#access'] = FALSE;
  $form['bc_value_sub']['#access'] = FALSE;

  $defaults = array(
    'Address' => array(
      'address' => '',
      'phone' => array(array('type' => '', 'number' => '')),
      'email' => ''
    ),
    'ShipAddress' => array(
      'address' => '',
      'phone' => array(array('type' => '', 'number' => '')),
      'email' => ''
    ),
    'Phone' => '',
    'Email' => ''
  );
  $data = isset($entity->data) ? json_decode($entity->data, TRUE) : NULL;
  $data = isset($data['data']) ? $data['data'] + $defaults : $defaults;
  foreach ($data as $key => $value) {
    if (is_array($value)) {
      $groupname = '-data-' . $key;
      $collapsedstate = TRUE;
      $form[$groupname] = array(
        '#type' => 'fieldset',
        '#title' => t($key . ' data'),
        '#collapsible' => TRUE,
      );
      $grouparray = array();
      _bc_flatten_array($value, $grouparray, $groupname);

      foreach ($grouparray as $groupkey => $groupvalue) {
        $prettytitle = explode('-', $groupkey);
        array_shift($prettytitle);
        array_shift($prettytitle);
        array_shift($prettytitle);
        $prettytitle = ucfirst(implode(' ', $prettytitle));
        $form[$groupname][$groupkey] = array(
          '#type' => 'textfield',
          '#title' => t($prettytitle),
          '#default_value' => $groupvalue,
        );
        $collapsedstate |= $groupvalue ? TRUE : FALSE;
      }
      $form[$groupname]['#collapsed'] = $collapsedstate;
    }
    else {
      $form['-data-' . $key] = array(
        '#type' => 'textfield',
        '#title' => t($key . ' data'),
        '#default_value' => $value,
      );
    }
  }
}