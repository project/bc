<?php

$plugin = array(
  'name' => 'payment method',
  'subtypes' => array(
    'cash',
    'check',
    'master card',
    'visa',
    'american express',
    'debit visa',
    'discover',
    'gift card',
    'paypal',
    'bitcoin',
    'wire transfer',
    'other credit card',
    'financing',
    'ach',
    'e check',
    'eft',
    'other'
  ),
);
