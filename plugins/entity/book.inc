<?php

$plugin = array(
  'name' => 'book',
  'form alter book' => array(
    // 'file' => 'filename', // defaults to this file
    // 'path' => 'filepath', // optional, will use plugin path if absent
    'function' => '_bc_form_alter_book',
  ),
  'form validate book' => array(
    // 'file' => 'filename', // defaults to this file
    // 'path' => 'filepath', // optional, will use plugin path if absent
    'function' => '_bc_form_validate_book',
  ),
);

/**
 * Implements ctools 'bc form alter' callback
 * @param $form
 * @param $entity
 */
function _bc_form_alter_book(&$form, $entity) {
  $form['bookid']['#access'] = TRUE;

  // find users with administer own beancount permissions and make select list
  $selectlist = _bc_users_with_beancount_owning_privileges();
  $form['uid'] = array(
    '#type' => 'select',
    '#title' => 'Book owner',
    '#options' => $selectlist,
    '#required' => TRUE,
    '#default_value' => $entity->uid,
  );
}

/**
 * Implements bc:entity 'form validate %bundle' ctools callback
 * @param $form
 * @param $entity
 */
function _bc_form_validate_book(&$form, $form_state) {
  if (!module_exists('qbxml')) {
    form_set_error('BC Entity', t('You must enable Quickbooks XML (qbxml) module in order to turn on QB sync functionality.'));
  }
  if (empty($form_state['entity']->bcid)) { // validating existing node
    $bookid = $form_state['values']['bookid'];
    $source = db_select('bc', 'b')
      ->condition('b.bookid', $bookid)
      ->condition('b.type', 'book')
      ->fields('b')
      ->execute();
    $bookexists = $source->rowCount();
    // $query = (string) $source;
    if ($bookexists) {
      form_set_error('BC Entity', t('A book number :bookid already exists. Choose a different book ID', array(':bookid' => $bookid)));
    }
  }
}