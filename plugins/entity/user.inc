<?php
$plugin = array(
  'name' => 'user',
  'form alter user' => array(
    // 'file' => 'filename', // defaults to this file
    // 'path' => 'filepath', // optional, will use plugin path if absent
    'function' => '_bc_form_alter_user',
  ),
);

/**
 * Implements ctools 'form alter' callback
 * @param $form
 * @param $entity
 * @return mixed
 */
function _bc_form_alter_user(&$form, $entity) {
  $form['ref'] = array(
    '#type' => 'textfield',
    '#title' => t('User ID to have access to this Book'),
    '#default_value' => $entity->ref ??  1,
    '#element_validate' => array('element_validate_integer_positive'),
    // TODO: make this validate against valid user IDs
  );

  $form['name']['#access'] = FALSE;
  $form['description']['#access'] = FALSE;

  $data = isset($entity->data) ? json_decode($entity->data, TRUE) : array();

  $rolelist = _bc_get_roles();
  asort($rolelist);
  $form['-data-roles'] = array(
    '#type' => 'select',
    '#title' => t('Roles of this user'),
    '#multiple' => TRUE,
    '#options' => array_combine($rolelist, $rolelist),
    '#default_value' => $data['roles'] ?? array(),
    // $entity->name,
  );

  return ($form);
}


/**
 * Implements bc:entity 'form validate %bundle' ctools callback
 * @param $form
 * @param $entity
 */
function _bc_form_validate_user(&$form, $form_state) {
  $uid = $form_state['values']['ref'];
  if (!($user = user_load($uid))) {
    form_set_error('BC Entity', t('The specified user ID :uid does not exist.'), array(':uid' => $uid));
  }
  else {
    $form_state['values']['name'] = t($user->name);
  }
}


