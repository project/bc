<?php


$plugin = array(
  'name' => 'item',
  'subtypes' => array(
    'item discount',
    'item fixed asset',
    'item group',
    'item inventory',
    'item inventory assembly',
    'item non inventory',
    'item other charge',
    'item payment',
    'item sales tax',
    'item sales tax group',
    'item service',
    'item subtotal',
    'item'
  ),
  'sync to external' => 'quickbooks',
);
