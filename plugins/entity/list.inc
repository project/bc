<?php

$plugin = array(
  'name' => 'list',
  'form alter list' => array(
    // 'file' => 'filename', // defaults to this file
    // 'path' => 'filepath', // optional, will use plugin path if absent
    'function' => '_bc_form_alter_list',
  ),
  'subtypes' => array(
      'billing rate',
      'class',
      'customer msg',
      'customer type',
      'date driven terms',
      'job type',
      'payment method',
      'price level',
      'sales tax code',
      'ship method',
      'standard terms',
      'terms',
      'vendor type',
    ),
  'sync to external' => 'quickbooks',
);

/**
 * Implements ctools 'form alter' callback
 * @param $form
 * @param $entity
 * @return mixed
 */
function _bc_form_alter_list(&$form, $entity) {
  $form['description']['#access'] = FALSE;

  $data = isset($entity->data) ? json_decode($entity->data, TRUE) : array();
  $form += array(
    '-data-stdduedays' => array(
      '#type' => 'textfield',
      '#title' => t('Standard due days'),
      '#default_value' => empty($data['stdduedays']) ? NULL : t($data['stdduedays']),
      '#element_validate' => array('element_validate_integer'),
    ),
    '-data-stddiscountdays' => array(
      '#type' => 'textfield',
      '#title' => t('Standard discount days'),
      '#default_value' => empty($data['stdduedays']) ? NULL : t($data['stddiscountdays']),
      '#element_validate' => array('element_validate_integer'),
    ),
    '-data-dayofmonthdue' => array(
      '#type' => 'textfield',
      '#title' => t('Day of month when due'),
      '#default_value' => empty($data['dayofmonthdue']) ? NULL : t($data['dayofmonthdue']),
      '#element_validate' => array('element_validate_integer'),
    ),
    '-data-duenextmonthdays' => array(
      '#type' => 'textfield',
      '#title' => t('Due the next month if issued within this many days of due date'),
      '#default_value' => empty($data['duenextmonthdays']) ? NULL : t($data['duenextmonthdays']),
      '#element_validate' => array('element_validate_integer'),
    ),
    '-data-discountdayofmonth' => array(
      '#type' => 'textfield',
      '#title' => t('Discount if paid before this day of the month'),
      '#default_value' => empty($data['discountdayofmonth']) ? NULL : t($data['discountdayofmonth']),
      '#element_validate' => array('element_validate_integer'),
    ),
    '-data-discountpct' => array(
      '#type' => 'textfield',
      '#title' => t('Discount percentage'),
      '#default_value' => empty($data['discountpct']) ? NULL : t($data['discountpct']),
      '#element_validate' => array('element_validate_number'),
    ),
  );
  $standardterms = empty($data['dayofmonthdue']) && empty($data['duenextmonthdays']) && empty($data['duenextmonthdays']);
  $datedriventerms = empty($data['stdduedays']) && empty($data['stdduedays']);
  if (!($standardterms && $datedriventerms)) {
    if ($standardterms) {
      $form['-data-dayofmonthdue']['#access'] = FALSE;
      $form['-data-duenextmonthdays']['#access'] = FALSE;
      $form['-data-discountdayofmonth']['#access'] = FALSE;
    }
    else {
      $form['-data-stdduedays']['#access'] = FALSE;
      $form['-data-stddiscountdays']['#access'] = FALSE;
    }
  }
}