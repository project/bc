<?php

$plugin = array(
  'name' => 'role',
  'form alter role' => array(
    // 'file' => 'filename', // defaults to this file
    // 'path' => 'filepath', // optional, will use plugin path if absent
    'function' => '_bc_form_alter_role',
  ),
  'subtypes' => array(
    'administrator',
    'accountant',
    'bookkeeper',
    'shipping',
    'parts picker',
    'receiving',
    'payroll',
    'sales',
    'marketing',
    'point of sale',
    'manager',
    'foreman',
    'tax preparer',
    'board of directors',
    'executive management',
    'vendor',
    'employee',
    'web administration',
    'web development',
    'web front-end',
    'web back-end',
    'web seo',
    'web advertisement',
  ),
);

/**
 * Implements ctools 'bc form alter' callback
 * @param $form
 * @param $entity
 * @return mixed
 */
function _bc_form_alter_role(&$form, $entity) {
  $default_roles = ctools_get_plugins('bc', 'entity', 'role')['subtypes'];
  $currentlyset = _bc_get_roles();
  $currentlyset = array_diff($currentlyset, array($entity->name));
  $selectfromthese = array_diff($default_roles, $currentlyset);
  if (!in_array($entity->name, $selectfromthese)) {
    $selectfromthese[] = $entity->name;
  }
  asort($selectfromthese);
  // this should probably not be an immutable list, but I wanted to give some guidance. This whole overloading of the 'name' field can just get axed if people complain too much.
  $form['name'] = array(
    '#type' => 'select',
    '#options' => array_combine($selectfromthese, $selectfromthese),
    '#default_value' => $entity->name,
    // $entity->name,
  );

  $data = isset($entity->data) ? json_decode($entity->data, TRUE) : array();
  // model at https://api.drupal.org/api/drupal/modules%21node%21node.admin.inc/function/node_admin_nodes/7.x
  $header = array(
    'property' => array('data' => t('Property'), 'field' => 'property')
  );
  foreach (bctypes() as $bundle) {
    $type = $bundle->type;
    $header[$type] = array('data' => t($bundle->label), 'field' => $type);
  }
  // <caption> doesn't exist for tableselect form elements, so we just throw a title in
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Field-level permissions for all Beancount entities.')
  );
  $optionkeys = array();
  foreach (bctypes() as $bundle) {
    $wrapper = entity_metadata_wrapper('bc', NULL, array('bundle' => $bundle->type));
    $info = $wrapper->getPropertyInfo();
    $editablefields = array();
    foreach (array_keys($info) as $key) {
      if ((!empty($info[$key]['setter permission']) &&
          ($info[$key]['setter permission'] == 'access beancount')
        ) ||
        !empty($info[$key]['field'])
      ) {
        $editablefields[$key] = '';
      }
    }
    $optionkeys += $editablefields;
  }
  $options = array();
  foreach (array_keys($optionkeys) as $optionkey) {
    foreach (array('r', 'w') as $RW) {
      $option = $optionkey . '-' . $RW;
      $options[$option]['property'] = array(
        'data' => array(
          '#type' => 'item',
          '#title' => ($RW == 'r') ? t($optionkey) : ''
        )
      );
      foreach (bctypes() as $bundle) {
        $type = $bundle->type;
        $options[$option][$type] = array();
        $wrapper = entity_metadata_wrapper('bc', NULL, array('bundle' => $type));
        $info = $wrapper->getPropertyInfo();
        if ((!empty($info[$optionkey]['setter permission']) &&
            ($info[$optionkey]['setter permission'] == 'access beancount')
          ) ||
          !empty($info[$optionkey]['field'])
        ) {
          $options[$option][$type] = array(
            'data' => array(
              '#type' => 'checkbox',
              '#title' => ucfirst($RW),
              '#checked' => $data[$type][$optionkey][$RW] ?? 0,
              // '#parents' => 'permissionstable',
              '#name' => $type . '[-data-' . $type . '-' . $optionkey . '-' . $RW . ']',
            )
          );
        }
      }
    }
  }

  $form['permissionstable'] = array(
    '#type' => 'tableselect',
    '#title' => t('Read/write permission.'),
    '#header' => $header,
    '#options' => $options,
    '#js_select' => TRUE,
    '#tree' => TRUE,
    '#caption' => t('Field-level permissions for all Beancount entities.'),
  );
  foreach (bctypes() as $bundle) {
    $form[$bundle->type] = array('#type' => 'value');
  }
  return ($form);
}
