<?php

class BcController extends EntityAPIController {

  public function create(array $values = array()) {
    global $user;
    $values += array(
      'name' => '',
      'description' => '',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
      'isactive' => TRUE,
      'ref' => 0,
    );
    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('bc', $entity);
    $content['author'] = array('#markup' => t('Created by: !author', array('!author' => $wrapper->uid->name->value(array('sanitize' => TRUE)))));

    // Make Description and Status themed like default fields.
    $content['description'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' => t('Description'),
      '#access' => TRUE,
      '#label_display' => 'above',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#entity_type' => 'bc',
      '#bundle' => $entity->type,
      '#items' => array(array('value' => $entity->description)),
      '#formatter' => 'text_default',
      0 => array('#markup' => check_plain($entity->description))
    );

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

  /**
   * Builds the query to load the entity.
   *
   * Differs from default controller by allowing conditions other than equality
   *
   * This has full revision support. For entities requiring special queries,
   * the class can be extended, and the default query can be constructed by
   * calling parent::buildQuery(). This is usually necessary when the object
   * being loaded needs to be augmented with additional data from another
   * table, such as loading node type into comments or vocabulary machine name
   * into terms, however it can also support $conditions on different tables.
   * See CommentController::buildQuery() or TaxonomyTermController::buildQuery()
   * for examples.
   *
   * @param $ids
   *   An array of entity IDs, or FALSE to load all entities.
   * @param $conditions
   *   An array of conditions. Keys are field names on the entity's base table.
   *   Values will be compared for equality. All the comparisons will be ANDed
   *   together. This parameter is deprecated; use an EntityFieldQuery instead.
   * @param $revision_id
   *   The ID of the revision to load, or FALSE if this query is asking for the
   *   most current revision(s).
   *
   * @return SelectQuery
   *   A SelectQuery object for loading the entity.
   *
   * protected function buildQuery($ids, $conditions = array(), $revision_id = FALSE) {
   * $query = db_select($this->entityInfo['base table'], 'base');
   *
   * $query->addTag($this->entityType . '_load_multiple');
   *
   * if ($revision_id) {
   * $query->join($this->revisionTable, 'revision', "revision.{$this->idKey} = base.{$this->idKey} AND revision.{$this->revisionKey} = :revisionId", array(':revisionId' => $revision_id));
   * }
   * elseif ($this->revisionKey) {
   * $query->join($this->revisionTable, 'revision', "revision.{$this->revisionKey} = base.{$this->revisionKey}");
   * }
   *
   * // Add fields from the {entity} table.
   * $entity_fields = $this->entityInfo['schema_fields_sql']['base table'];
   *
   * if ($this->revisionKey) {
   * // Add all fields from the {entity_revision} table.
   * $entity_revision_fields = drupal_map_assoc($this->entityInfo['schema_fields_sql']['revision table']);
   * // The id field is provided by entity, so remove it.
   * unset($entity_revision_fields[$this->idKey]);
   *
   * // Remove all fields from the base table that are also fields by the same
   * // name in the revision table.
   * $entity_field_keys = array_flip($entity_fields);
   * foreach ($entity_revision_fields as $key => $name) {
   * if (isset($entity_field_keys[$name])) {
   * unset($entity_fields[$entity_field_keys[$name]]);
   * }
   * }
   * $query->fields('revision', $entity_revision_fields);
   * }
   *
   * $query->fields('base', $entity_fields);
   *
   * if ($ids) {
   * $query->condition("base.{$this->idKey}", $ids, 'IN');
   * }
   * if ($conditions) {
   * foreach ($conditions as $field => $value) {
   * if (is_array($value) && isset($value['operator'])) {
   * $operator = $value['operator'];
   * unset($value['operator']);
   * $query->condition('base.' . $field, $value, $operator);
   * }
   * else {
   * $query->condition('base.' . $field, $value);
   * }
   * }
   * }
   * return $query;
   * }
   */
}

class BcTypeController extends EntityAPIControllerExportable {
  public function create(array $values = array()) {
    $values += array(
      'label' => '',
      'description' => '',
      'isactive' => TRUE,
    );
    return parent::create($values);
  }

  /**
   * Save Entity Type.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    parent::save($entity, $transaction);
    // Rebuild menu registry. We do not call menu_rebuild directly, but set
    // variable that indicates rebuild in the end.
    // @see http://drupal.org/node/1399618
    variable_set('menu_rebuild_needed', TRUE);
  }
}

/**
 * UI controller for Entity Type.
 */
class BcTypeUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage Beancount Entity Types.';
    return $items;
  }
}

/**
 * Entity class.
 */
class Bc extends Entity {
  protected function defaultLabel() {
    return $this->name;
  }

  public function typelabel() {
    $typelabel = $this->entityInfo['bundles'][$this->type]['label'];
    return (t($typelabel));
  }

  protected function defaultUri() {
    return array('path' => 'bc/' . $this->identifier());
  }
}

/**
 * Entity Type class.
 */
class BcType extends Entity {
  public $type;
  public $label;
  public $weight = 0;

  public function __construct($values = array()) {
    parent::__construct($values, 'bctype');
  }

  function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}