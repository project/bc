<?php
/**
 * Created by: hugh
 * Date: 12/21/16
 * Time: 11:12 AM
 */

$fmtdef = array(
  array(
    'value' => array(
      'extid' => array('key' => 'ListID',),
      'bcid' => array('key' => 'EditSequence',),
      // use bcid because this gets looked up using editsequence callback when QBXML gets generated
      'name' => array('key' => 'Name',),
      'isactive' => array(
        'key' => 'IsActive',
        'theme' => 'qbxml_bool_truefalse'
      ),
      'bc_parent' => array(
        'key' => array('ParentRef', 'ListID'),
        'theme' => 'qbxml_bc_reference'
      ),
      'bc_type' => array(
        'key' => 'AccountType',
        'theme' => 'qbxml_camel_term_name'
      ),
    ),
    'data' => array(
      'AccountNumber' => array(),
      'BankNumber' => array(),
    ),
  ),
  array(
    'value' => array(
      'description' => array('key' => 'Desc',),
    ),
    'data' => array(
      'OpenBalanceDate' => array(),
      'TaxLineID' => array(),
    )
  ),
);

$outputarray = _bc_theme_generate_output_array($values, $fmtdef);
$wraptag = (!empty($qbxml_base) && !empty($qbxml_opcode))
  ? $qbxml_base . $qbxml_opcode
  : '';
print _qbxml_array_to_xml($outputarray, $wraptag);
