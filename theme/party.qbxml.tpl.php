<?php
/**
 * Created by: hugh
 * Date: 12/21/16
 * Time: 11:12 AM
 */

$fmtdef = array(
  array(
    'value' => array(
      'extid' => array('key' => 'ListID',),
      'bcid' => array('key' => 'EditSequence',),
      // use bcid because this gets looked up using editsequence callback when QBXML gets generated
      'name' => array('key' => 'Name',),
      'isactive' => array(
        'key' => 'IsActive',
        'theme' => 'qbxml_bool_truefalse'
      ),
    ),
    'bc ref' => array(
      'Class' => array(),
    ),
  ),
  array(
    'value' => array(
      'bc_parent' => array(
        'key' => array('ParentRef', 'ListID'),
        'theme' => 'qbxml_bc_reference'
      ),
    ),
    'data' => array(
      'CompanyName' => array(),
      'Salutation' => array(),
      'FirstName' => array(),
      'MiddleName' => array(),
      'LastName' => array(),
      'JobTitle' => array(),
      'Address' => array(
        'key' => 'BillAddress',
        'theme' => 'qbxml_bc_address'
      ),
      'ShipAddress' => array( /*'key' => 'ShipAddress',*/
        'theme' => 'qbxml_bc_address'
      ),
    ),
  ),
  array(
    'value' => array(
      '-data-Address-phone-0-number' => array('key' => 'Phone'),
    ),
    'data' => array(
      'Phone' => array(),
    )
  ),
  array(
    'value' => array(
      '-data-ShipAddress-phone-0-number' => array('key' => 'AltPhone'),
    ),
    'data' => array(
      'AltPhone' => array(),
      'Fax' => array(),
    )
  ),
  array(
    'value' => array(
      '-data-Address-email' => array('key' => 'Email')
    ),
    'data' => array(
      'Email' => array(),
      'Contact' => array(),
      'AltContact' => array(),
    ),
    'bc ref' => array(
      'CustomerType' => array(),
      'StandardTerms' => array('key' => 'Terms'),
      'DataDrivenTerms' => array('key' => 'Terms'),
      'SalesRep' => array(),
      'SalesTaxCode' => array(),
      'ItemSalesTax' => array()
    ),
  ),
  array(
    'data' => array(
      'ResaleNumber' => array(),
      'AccountNumber' => array(),
      'CreditLimit' => array(),
    ),
    'bc ref' => array(
      'PreferredPaymentMethod' => array(),
      'PriceLevel' => array(),
      'Currency' => array()
    ),
  ),
);

$outputarray = _bc_theme_generate_output_array($values, $fmtdef);
$wraptag = (!empty($qbxml_base) && !empty($qbxml_opcode))
  ? $qbxml_base . $qbxml_opcode
  : '';
print _qbxml_array_to_xml($outputarray, $wraptag);
