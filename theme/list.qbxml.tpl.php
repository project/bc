<?php
/**
 * Created by: hugh
 * Date: 12/21/16
 * Time: 11:12 AM
 */
$terms = array(
  'extid' => array('key' => 'ListID',),
  'bcid' => array('key' => 'EditSequence',),
  // use bcid because this gets looked up using editsequence callback when QBXML gets generated
  'name' => array('key' => 'Name',),
  'isactive' => array('key' => 'IsActive', 'theme' => 'qbxml_bool_truefalse'),
  'description' => array('key' => 'Desc',),
  '-data-stdduedays' => array('key' => 'StdDueDays',),
  '-data-stddiscountdays' => array('key' => 'StdDiscountDays',),
  '-data-dayofmonthdue' => array('key' => 'DayOfMonthDue',),
  '-data-duenextmonthdays' => array('key' => 'DueNextMonthDays',),
  '-data-discountdayofmonth' => array('key' => 'DiscountDayOfMonth',),
  '-data-discountpct' => array('key' => 'DiscountPct'),
);

$outputarray = _bc_theme_generate_output_array($values, $fmtdef);
$wraptag = (!empty($qbxml_base) && !empty($qbxml_opcode))
  ? $qbxml_base . $qbxml_opcode
  : '';
print _qbxml_array_to_xml($outputarray, $wraptag);
