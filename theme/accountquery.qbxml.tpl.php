<?php
/**
 * Created by: hugh
 * Date: 12/21/16
 * Time: 11:12 AM
 *
 * $terms = array(
 * 'activestatus' =>       array( 'key' => 'ActiveStatus', ),
 * 'timedatefrom' =>   array( 'key' => 'FromModifiedDate', ),
 * 'timedateto' =>     array( 'key' => 'ToModifiedDate', ),
 * 'name' =>               array( 'key' => 'FullName', ),
 * 'bc_class' =>           array( 'key' => 'AccountType', 'theme' => 'qbxml_field_value'),
 * // TODO: parent ref goes here when implemented
 * // NameFilter
 * // NameRangeFilter
 * // IncludeRetElement
 * );
 * $output = '';
 * foreach( $terms as $key => &$format ) {
 * if( isset($values[$key]) ) {
 * $value = isset($format['theme']) ? theme($format['theme'], array( 'value' => $values[$key] )) : $values[$key];
 * if( !is_null($value) ) {
 * if ($value) {
 * $output .= '<' . $format['key'] . '>' . $value . '</' . $format['key'] . '>';
 * }
 * } // need something more sophisticated for nested NameFilter and NameRangeFilter and IncludeRetElement plus the order is probably significant
 * }
 * }
 *
 * print $output;*/

$fmtdef = array(
  array(
    'value' => array(
      'activestatus' => array('key' => 'ActiveStatus'),
      'timedatefrom' => array('key' => 'FromModifiedDate',),
      'timedateto' => array('key' => 'ToModifiedDate',),
      'name' => array('key' => 'FullName',),
      'bc_class' => array(
        'key' => 'AccountType',
        'theme' => 'qbxml_field_value'
      ),
    )
  ),
);

$outputarray = _bc_theme_generate_output_array($values, $fmtdef);
$wraptag =
  /*(!empty($qbxml_base) && !empty($qbxml_opcode))
  ? $qbxml_base . $qbxml_opcode
  : */
  '';
print _qbxml_array_to_xml($outputarray, $wraptag);
