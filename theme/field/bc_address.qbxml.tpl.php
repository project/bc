<?php

// print $value;
$outputarray = array();
$i = 1;
if( isset($value['address']) ) {
  $addresslines = explode('; ', $value['address']);
  foreach( $addresslines as $addressline ) {
    $outputarray['Addr' . $i++] = $addressline;
  }
  if( !empty($value['phone'][0]['number']) ) {
    $phonetag = ($i < 6) ? 'Addr' . $i : 'Note';
    $prefix = !empty($value['phone'][0]['type']) ? $value['phone'][0]['type'] . ':' : '';
    $outputarray[$phonetag] = $prefix . $value['phone'][0]['number'];
  }
  $i++;
  if( !empty($value['email']) ) {
    $tag = ($i < 6) ? 'Addr' . $i : 'Note';
    $outputarray[$tag] = $value['email'];
  }
}

print _qbxml_array_to_xml($outputarray, '');
/*
if( isset($value['address'][0]['target_id']) ) {
  if( $target_id = $value['und'][0]['target_id'] ) {
    if( $term = bc_load($target_id) ) {
      $vals = explode( '::', $term->extid );
      if( !empty($vals[1]) ) { // todo: it is possible to have a reference to a new beancount that does not yet have an extid. this needs to be resolved / cleaned up somehow
        print $vals[1];
      }
    }
  }
}
*/