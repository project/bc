<?php

if( isset($value['und'][0]['target_id']) ) {
  if( $target_id = $value['und'][0]['target_id'] ) {
    if( $term = bc_load($target_id) ) {
      $vals = explode( '::', $term->extid );
      if( !empty($vals[1]) ) { // todo: it is possible to have a reference to a new beancount that does not yet have an extid. this needs to be resolved / cleaned up somehow
        print $vals[1];
      }
    }
  }
}
