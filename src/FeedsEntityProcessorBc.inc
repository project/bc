<?php

/**
 * @file
 * Contains FeedsEntityProcessor.
 */

/**
 * Creates entities from feed items.
 */
class FeedsEntityProcessorBc extends FeedsEntityProcessor {

  /**
   * Returns the entity type.
   *
   * @return string
   *   The type of entity this processor will create.
   */
  public function entityType() {
    return 'bc';
  }

  /**
   * Overrides parent::entityInfo().
   */
  protected function entityInfo() {
    $info = parent::entityInfo();
    $info['label plural'] = isset($info['label plural']) ? $info['label plural'] : $info['label'] . 's';

    return $info;
  }

  /**
   * Return default configuration.
   *
   * @todo rename to getConfigDefaults().
   *
   * @return
   *   Array where keys are the variable names of the configuration elements and
   *   values are their default values.
   */
  public function configDefaults() {
    return array('config' => array('values' => array('bcid' => '')));
  }

}
