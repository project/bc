CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Beancount module provides entities for bookkeeping, financial, and enterprise-management purposes.

It comes with feeds importers that activate automatically when using the QBWC and QBXML modules to import data from Quickbooks.

REQUIREMENTS
------------

Beancount uses the Amount module, which requires that your system be 64-bit.

Beancount also uses some patches that have not been incorporated into Drupal. Please install the patches for:

* Issue 1621356: Pass all the parameters of hook_options_list() to options_list_callback
* Issue 1340098: Move target bundle to the field instance settings

RECOMMENDED MODULES
-------------------

If Views is installed. The data will be presented in organization by Beancount type.

Quickbooks XML is strongly recommended in order to provide data synchronization with a Quickbooks site.

INSTALLATION
------------

The Beancount module is installed just like any other Drupal module. Use the admin interface or drush.

CONFIGURATION
-------------

Beancount requires no configuration. Bundles may be modified using the structure interface.

MAINTAINERS
-----------

Current maintainers:
 * Hugh Kern (geru) - https://drupal.org/user/2778683

