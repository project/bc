<?php

/**
 * Implements hook_views_default_views().
 */
function bc_views_default_views() {
  
  foreach( array( 'account', 'list', 'item', 'party' ) as $base ) {
    if( $v = views_get_view('bc_' . $base) ) {
      // while dev, we'll delete each time, later, views persist and we return here
      views_delete_view( $v );
    }
    if( $v = views_get_view('bc' . $base) ) {
      // while dev, we'll delete each time, later, views persist and we return here
      views_delete_view( $v );
    }
  }
  
  $views = array();
  $files = file_scan_directory(__DIR__, '/(\w+)\.views.export$/');
  foreach ($files as $file) {
    $defstr = file_get_contents($file->uri);
    $export = NULL;
    ob_start();
    eval($defstr);
    ob_end_clean();
    if (isset($view)) {
      if( $v = views_get_view( $view->name ) ) {
        views_delete_view($v);
      }
      $views[$view->name] = $view;
    }
  }

  $files = file_scan_directory(__DIR__, '/beancount.export.clone$/');
  $file = reset($files);
  foreach( array('account', 'party', 'list', 'item') as $listname ) {
    $defstr = file_get_contents($file->uri);
    preg_match('|^(.*)(/\* Display: P:Account:Type \*/.*)$|s', $defstr, $match);
    // first match is view and Account display information,
    // second match is
    // third match is Account subtype / accounts payable display
    $listmachine = strtr($listname, array(' ' => ''));
    $genstr = preg_replace('/account/s', $listmachine, $match[1]);
    $genstr = preg_replace('/Account/s', ucfirst($listname), $genstr);

    $pagenum = 1;
    $subtypelist = _bc_list_subtype($listname);
    foreach ($subtypelist as $subtype) {
      $sublistmachine = strtr($subtype, array(' ' => ''));
      $sublistindex = _bc_list_subtype($listname, $subtype);
      $tmppage = preg_replace('/accountspayable/', "$sublistmachine", $match[2]);
      $tmppage = preg_replace('/Accounts payable/', ucfirst($subtype), $tmppage);
      $tmppage = preg_replace('/Account:Type/', ucfirst($listname) . ':' . ucfirst($subtype), $tmppage);
      $tmppage = preg_replace("/\['filters'\]\['subtype'\]\['value'\] = '\d+';/s", "['filters']['subtype']['value'] = '$sublistindex';", $tmppage);
      $tmppage = preg_replace('/account/s', $listmachine, $tmppage);
      $tmppage = preg_replace('/Account/s', ucfirst($listname), $tmppage);
      $tmppage = preg_replace("'page_\d+'", "page_$pagenum", $tmppage);
      $pagenum++;
      $genstr .= $tmppage;
    }
    $export = NULL;
    ob_start();
    eval($genstr);
    ob_end_clean();
    if (isset($view)) {
      $views[$view->name] = $view;
    }
  }

  return ($views);
}