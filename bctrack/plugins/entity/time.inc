<?php

$plugin = array(
  'name' => 'time',
  'form alter time' => array(
    // 'file' => 'filename', // defaults to this file
    // 'path' => 'filepath', // optional, will use plugin path if absent
    'function' => '_bctime_form_alter_time',
  ),
  'subtypes' => array(
    'timecard',
    'project',
    'event',
  ),
  'entity info' => array(
    'type' => 'time',
    'label' => 'Time',
    'uuid' => '5da2a3f7-447d-4803-8f36-ad940221b990',
    'description' => 'A Beancount time tracker.'
  ),
);

/**
 * Implements ctools 'bc form alter' callback
 * @param $form
 * @param $entity
 */
function _bctime_form_alter_time(&$form, $entity) {
  $data = isset($entity->data) ? json_decode($entity->data, TRUE) : array();
  $form['-data-start'] = array(
      '#type' => 'textfield',
      '#title' => t('Start time'),
      '#disabled' => TRUE,
      '#default_value' => isset($data['start']) ? $data['start'] : $entity->created
  );
  if( isset($data['start'])) {
    $form['-data-end'] = array(
      '#type' => 'textfield',
      '#title' => t('End time'),
      '#disabled' => TRUE,
      '#default_value' => isset($data['end']) ? $data['end'] : $entity->changed
    );
  }
  $form['name']['#disabled'] = TRUE;
  $form['description']['#disabled'] = TRUE;
  $form['isactive']['#disabled'] = TRUE;
}