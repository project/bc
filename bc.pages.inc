<?php

/**
 * Entity view callback.
 */
function bc_view($entity) {
  drupal_set_title(entity_label('bc', $entity));
  return entity_view('bc', array(entity_id('bc', $entity) => $entity));
}
