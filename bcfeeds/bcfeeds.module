<?php

require_once 'bcfeeds.inc';

/**
 * Implements hook_feeds_tamper_extras_importdir().
 */
function bcfeeds_feeds_tamper_extras_importdir() {
  return __DIR__ . '/importers';
}

/**
 * Update extid in db from name on Add responses on the fly when processing import
 * @param $qbxmlrs QBXML response tag
 * @param $bookid
 * @param $bundlename
 * @param $ref
 * @param $name
 * @param $extid newly discovered extid
 */
function _bcfeeds_update_extid_by_name($opcode, $bookid, $bundlename, $ref, $name, $extid) {
  if ($opcode == 'Add') {
    if ($query = db_update('bc')
      ->condition('bookid', $bookid)
      ->condition('type', $bundlename)
      ->condition('ref', $ref)
      ->condition('name', $name)
      ->fields(array('extid' => $extid))
      ->execute()
    ) {
      // if this fails, then someone deleted the record and a new one gets generated, in the case of subtype changing or parent move, a duplicate record will get inserted into db
    }
  }
  return;
}

function _bcfeeds_debug($var, $default, $field, $result, $item_key, $element_key, $settings, $source) {
  if (isset($field)) {
    return NULL;
  }
}

function _bcfeeds_addressprocessing($result = NULL) {
  if ($result) {
    $ret = array();
    $address = $result;
    if (!is_array($address)) {
      $address = array($address);
    }
    foreach ($address as $addr) {
      $matches = array();
      // check for email address match
      if (preg_match_all('/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}/', strtolower($addr), $matches)) {
        foreach ($matches as $match) {
          $ret['email'][] = $match[0];
        }
        continue;
      }
      if (!preg_match('/(.*?)(\d{5})([-](\d{4}))?/', $addr)) {
        // check for telephone number match
        if (preg_match_all('/(\D*)(\d+)[-) ]*((\d+)[- ]+(\d+))+/', $addr, $matches)) {
          foreach ($matches[0] as $key => $match) {
            $type = array();
            $phone = array();
            if (preg_match('/\W*(\w*)/', $matches[1][$key], $type)) {
              $phone['type'] = $type[1];
            }
            $phone['number'] = $matches[2][$key] . '-' . $matches[4][$key] . '-' . $matches[5][$key];
            $ret['phone'][] = $phone;
          }
          continue;
        }
      }
      if (!empty($ret['address'])) {
        $ret['address'] .= '; ' . $addr;
      }
      else {
        $ret['address'] = $addr;
      }
    }
    return $ret;
  }
  return NULL;
}

/**
 * Save extra data field for Beancount Party bundle
 * @param $var
 * @param $default
 * @return null|string
 */
function _bcfeeds_party_datafield($var) {
  $primaryaddress = array(
    'customer' => 'BillAddress',
    'vendor' => 'VendorAddress',
    'employee' => 'EmployeeAddress',
    'other name' => 'OtherNameAddress'
  );
  $secondaryaddress = array(
    'customer' => 'ShipAddress',
    'vendor' => 'ShipAddress',
    'other name' => 'AltAddress'
  ); // OtherNameAddressBlock might just mirror OtherNameAddress
  $arr = array();
  $data = array();
  $subterm = $var['xpathparser2'];
  if (($addr = _bcfeeds_addressprocessing($var['xpathparser13'])) && !empty($primaryaddress[$subterm])) {
    $data[$primaryaddress[$subterm]] = $addr;
  }
  if (($addr = _bcfeeds_addressprocessing($var['xpathparser14']) && !empty($secondaryaddress[$subterm]))) {
    $data[$secondaryaddress[$subterm]] = $addr;
  }
  if ($var['xpathparser15']) {
    $data['Phone'] = $var['xpathparser15'];
  }
  if ($var['xpathparser16']) {
    $data['AltPhone'] = $var['xpathparser16'];
  }
  if ($var['xpathparser17']) {
    $data['Email'] = $var['xpathparser17'];
  }
  if ($var['xpathparser18']) {
    $data['Contact'] = $var['xpathparser18'];
  }
  if ($var['Blanksource6']) {
    $arr['parentextid'] = $var['xpathparser9'];
  }
  if ($data) {
    $arr['data'] = $data;
  }
  $cleanedarray = array_filter($arr);
  return empty($cleanedarray) ? NULL : json_encode($cleanedarray);
}

function _bcfeeds_list_datafield($var) {
  $arr = array();
  $data = array();
  if ($var['xpathparser11']) {
    $data['StdDueDays'] = $var['xpathparser11'];
  }
  if ($var['xpathparser12']) {
    $data['StdDiscountDays'] = $var['xpathparser12'];
  }
  if ($var['xpathparser13']) {
    $data['DayOfMonthDue'] = $var['xpathparser13'];
  }
  if ($var['xpathparser14']) {
    $data['DueNextMonthDays'] = $var['xpathparser14'];
  }
  if ($var['xpathparser15']) {
    $data['DiscountDayOfMonth'] = $var['xpathparser15'];
  }
  if ($var['xpathparser16']) {
    $data['PaymentMethodType'] = $var['xpathparser16'];
  }
  if ($var['xpathparser17']) {
    $data['PriceLevelType'] = $var['xpathparser17'];
  }
  if ($var['xpathparser19']) {
    $data['IsTaxable'] = $var['xpathparser19'];
  }
  if ($var['xpathparser21']) {
    $data['BillingRateType'] = $var['xpathparser21'];
  }
  if ($var['Blanksource6']) {
    $arr['parentextid'] = $var['xpathparser9'];
  }
  if ($data) {
    $arr['data'] = $data;
  }
  $cleanedarray = array_filter($arr);
  return empty($cleanedarray) ? NULL : json_encode($cleanedarray);
}

/**
 * For Items, stash extra data in serialized data field
 * @param $var
 * @return null|string
 */
function _bcfeeds_item_datafield($var) {
  $arr = array();
  $data = array();
  $taginfo = array();

  // Blanksource8 is structure from sub line-items (assembly or group?)
  //   xpathparser13 ->
  //   xpathparser21 -> quantity
  // has potentially multiple line items in it
  if ($var['Blanksource8']) {
    $data += $var['Blanksource8'];
  }

  // save parentextid info whether ref resolved or not
  // x9 is parent extid
  if ($var['Blanksource6']) {
    $arr['parentextid'] = $var['xpathparser9'];
  }

  // extra processing gets stimulated on certain item types
  if (_bcfeeds_item_type_gets_extra_processing($var['Blanksource7'])) {
    // x7 is Name
    if (!empty($var['xpathparser7'])) {
      $taginfo['sku'] = $var['xpathparser7'];
    }
    // x14 is description field, first line maps to Name fields
    //   second line and beyond are YAML / JSON formatted extra info
    if ($var['xpathparser14']) {
      $array = preg_split("/\r\n|\n|\r/", $var['xpathparser14']);
      $taginfo['desc'] = array_shift($array);
      if ($varstr = implode(PHP_EOL, $array)) {
        $taginfo['desc'] = function_exists('yaml_parse')
          ? yaml_parse($varstr)
          : json_decode($varstr, TRUE);
      }
    }
    if ($data) {
      $arr['data'] = $data;
    }
    if ($taginfo) {
      $arr['taginfo'] = $taginfo;
    }
  }
  $cleanedarray = array_filter($arr);
  return empty($cleanedarray) ? NULL : json_encode($cleanedarray);
}

/*
 * Take an input term in CamelForm and convert to a lowercase string of terms separated by a space and missing the last term
 */
function _bcfeeds_subtypefromcamel($field) {
  $arr = qbxml_camelterm_to_array($field);
  array_pop($arr); // remove Ret from end
  return strtolower(implode(' ', $arr)); // sub term
}

/**
 * If an element is in search-list, then it is a percentage.
 * @param $needle
 * @param $haystack
 * @return null|string
 */
function _bcfeeds_setunitspct($needle, $haystack) {
  if (in_array($needle, $haystack)) {
    return 'AMT_PCT_%_';
  }
  return NULL;
}

/**
 * build an array from columns
 * Where input has multiple / variable number of items, this constructs an array of those items
 * @param $columns
 * @return array|null
 */
function _bcfeeds_lineitemarray($columns) {
  if (!$columns) {
    return NULL;
  }
  $keys = array();
  // get keys and arrayify each column
  foreach ($columns as $name => &$column) {
    $keys[] = $name;
    if (!is_array($column)) {
      $column = array($column);
    }
  }
  $ret = array();
  foreach ($columns[$keys[0]] as $index => $value) {
    $build = array();
    foreach ($keys as $key) {
      $build[$key] = $columns[$key][$index];
    }
    $ret[] = $build;
  }
  return $ret ? $ret : NULL;
}

/*
 * flag item types that get extra taxonomy processing
 */
function _bcfeeds_item_type_gets_extra_processing($type) {
  return in_array($type, array(
    'item inventory',
    'item group',
    'item other charge',
    'item assembly',
    'item non inventory',
    'item service'
  ));
}