CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

 INTRODUCTION
 ------------

The Beancount Feeds module provides Feeds Importers, Feeds Tampers, and Feeds manipulation functionality for the
Beancount module.

Importers and Tampers are defined as export files, and are instantiated and destroyed by the Feeds Tamper Extras module.

Currently defined Importers and Tampers bring in data from Quickbooks. Other importers can be defined.


REQUIREMENTS
------------

This module requires the following modules:

 * Feeds
 * Feeds Tamper
 * Feeds Tamper Extras
 * Feeds Entity Processor
 * Feeds XPath Parser
 * Beancount

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will create its Feeds Importers and Tampers. When it is disabled, the module will destroy them.

MAINTAINERS
-----------

Current maintainers:
 * Hugh Kern (geru) - https://drupal.org/user/2778683

