<?php

/**
 * Implements hook qbxml_companyinfo()
 * @param $bookid
 * @param $response
 * @param $data
 */
function bcfeeds_qbwc_companyInfo($bookid, $data) {
  db_update('bc')
    ->condition('bc.bookid', $bookid)
    ->condition('bc.type', 'book')
    ->fields(array('data' => json_encode($data)))
    ->execute();
}