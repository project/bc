<?php
/**
 * Created by: hugh
 * Date: 12/3/16
 * Time: 8:09 PM
 */

require_once 'bcfeeds.inc';

/**
 * Implements hook_feeds_presave()
 * @param \FeedsSource $source
 * @param $entity
 * @param $item
 * @param $entity_id
 */
function bcfeeds_feeds_presave(FeedsSource $source, $entity, $item, $entity_id) {
  // go through all fields and screen for xss
  foreach ($entity as $key => &$value) {
    if (is_string($value)) {
      $value = filter_xss($value);
    }
  }
  return;
}

/*
function bcfeeds_before_update(FeedsSource $source, $item, $entity_id) {
  // screen for xss
  return;
}
*/

/**
 * return an array element index for a position
 */
function _bcfeeds_array_element($pos, &$array) {
  $idx = ($pos >= 0) ? $pos : count($array) + $pos;
  return isset($array[$idx]) ? $idx : NULL;
}

function _bcfeeds_create_taxonomy_term($code, $name, $vocabulary_machine) {
  $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_machine);
  $term = new stdClass();
  $term->name = $code;
  $term->description = $name;
  $term->vid = $vocabulary->vid;
  taxonomy_term_save($term);
  return $term->tid;
}

/**
 * Get a term id if it exists and if it doesn't, create it and return it
 * @param $name
 * @param $desc
 * @param $vocab_machine
 * @return mixed
 */
function _bcfeeds_get_tid($name, $desc, $vocab_machine) {
  if ($terms = taxonomy_get_term_by_name($name, $vocab_machine)) {
    $tid = reset($terms)->tid;
  }
  else {
    $tid = _bcfeeds_create_taxonomy_term($name, $desc, $vocab_machine);
  }
  return ($tid);
}

/**
 * Process tags for an item
 */
function _bcfeeds_item_process_tags(&$item, &$taginfo, $fmt = array()) {
  $ret = FALSE;
  if (!isset($taginfo['sku'])) {
    return $ret;
  }
  $fmt = $fmt + array(
      'delim' => array(
        'sku' => '-',
        'desc' => ' - ',
        'model' => '-',
      ),
      'pos' => array(
        'brand' => -1,
        'sku' => 0,
      ),
    );

  $desc = isset($taginfo['desc']) ? $taginfo['desc'] : '';
  $parsearray = array(
    'sku' => explode($fmt['delim']['sku'], $taginfo['sku']),
    'desc' => explode($fmt['delim']['desc'], $desc)
  );
  $info = array();
  $delete = array('sku' => array(), 'desc' => array());

  foreach (array('sku', 'brand') as $type) {
    $idx = _bcfeeds_array_element($fmt['pos'][$type], $parsearray['sku']);
    if (($idx !== NULL) && !in_array($idx, $delete['sku'])) {
      $info[$type] = array('code' => strtolower($parsearray['sku'][$idx]));
      $delete['sku'][] = $idx;
      $idx = _bcfeeds_array_element($fmt['pos'][$type], $parsearray['desc']);
      if ($idx !== NULL) {
        $info[$type]['name'] = $parsearray['desc'][$idx];
        $delete['desc'][] = $idx;
      }
    }
  }
  foreach (array('sku', 'desc') as $parsetype) {
    foreach ($delete[$parsetype] as $idx) {
      unset($parsearray[$parsetype][$idx]);
    }
  }

  if ($parsearray['sku']) {
    $info['model'] = array('code' => implode($fmt['delim']['sku'], $parsearray['sku']));
    if ($parsearray['desc']) {
      $info['model']['name'] = implode($fmt['delim']['desc'], $parsearray['desc']);
    }
  }

  $item->data['item'] = $info;

  // now, we have brand, product, and model information
  if (!empty($info['brand']['code'])) {
    $tid = _bcfeeds_get_tid($info['brand']['code'], $info['brand']['name'], 'bc_item_brand');
    $item->bc_item_brand = array('und' => array(array('tid' => $tid)));
    $ret = TRUE;
  }

  // do same thing for product code / description
  if (!empty($info['sku']['code'])) {
    $tid = _bcfeeds_get_tid($info['sku']['code'], $info['sku']['name'], 'bc_item_sku_code');
    $item->bc_item_sku_code = array('und' => array(array('tid' => $tid)));
    $ret = TRUE;
  }

  // do same thing for product name description
  if (!empty($info['sku']['name'])) {
    $skunames = array('tid' => _bcfeeds_get_tid(strtolower($info['sku']['name']), $info['sku']['code'], 'bc_item_sku_name'));
    // split the name into words
    if (strpos($info['sku']['name'], ' ')) {
      $words = explode(' ', $info['sku']['name']);
      foreach ($words as $word) {
        $word = rtrim($word, '.:;<>?&*[]{},-/=()@!~+_');
        $skunames = array('tid' => _bcfeeds_get_tid(strtolower($word), $info['sku']['code'], 'bc_item_sku_name'));
      }
    }
    $item->bc_item_sku_name = array('und' => array($skunames)); // check if this is how it is done
    $ret = TRUE;
  }

  return $ret;
}

/**
 * Implements hook_feeds_after_import()
 * @param \FeedsSource $source
 * @param $entity
 * @param $item
 * @param $entity_id
 */
function bcfeeds_feeds_after_import(FeedsSource $source) {
  $entities2save = array();
  $entities = bc_load_multiple(FALSE, array('state' => array(1, 3)));
  foreach ($entities as &$orphan) {
    $data = json_decode($orphan->data, TRUE);
    $ref = db_select('bc', 'b')
      ->condition('b.extid', $data['parentextid'])
      ->fields('b', array('bcid')
      )->execute()->fetchField();
    if ($ref) {
      $orphan->ref = $ref;
      unset($data['parentextid']);
      $orphan->data = $data ? json_encode($data) : NULL;
      $orphan->state &= ~BCFEEDS_STATE_PARENTREFLOOKUP;
      $orphan->bc_parent = array('und' => array(array('target_id' => $ref)));
      $entities2save[$orphan->bcid] = $orphan;
    }
  }
//  $entities = bc_load_multiple(FALSE, array( 'state' => array(BCFEEDS_STATE_ITEMTERMSPROCESS, 'operator' => '&') ) );
  $entities = bc_load_multiple(FALSE, array('state' => array(2, 3)));
  foreach ($entities as $item) {
    if (isset($entities2save[$item->bcid])) {
      $item = $entities2save[$item->bcid];
    }
    $fmt = array();
    if ($item->ref) {
      $parent = bc_load($item->ref);
      $data = json_decode($parent->data, TRUE);
      if (isset($data['taginfo']['data'])) {
        $fmt = $data['taginfo']['data'];
      }
    }
    $taginfo = NULL;
    $data = json_decode($item->data, TRUE);
    if (isset($data['taginfo'])) {
      $taginfo = $data['taginfo'];
      if (isset($taginfo['data'])) {
        $fmt = $taginfo['data'] + $fmt;
      }
    }
    $item->data = $data;
    if (_bcfeeds_item_process_tags($item, $taginfo, $fmt)) {
      $item->state &= ~BCFEEDS_STATE_ITEMTERMSPROCESS;
      $item->data = json_encode($item->data);
      $entities2save[$item->bcid] = $item;
    }
  }
  foreach ($entities2save as $entity) {
    bc_save($entity);
  }
}
