<?php

/**
 * Generate the entity type editing form.
 * @param $form
 * @param $form_state
 * @param $entity_type
 * @param string $op
 * @return mixed
 */
function bctype_form($form, &$form_state, $entity_type, $op = 'edit') {

  if ($op == 'clone') {
    $entity_type->label .= ' (cloned)';
    $entity_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => t($entity_type->label),
    '#description' => t('The human-readable name of this entity type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

// Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($entity_type->type) ? $entity_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $entity_type->isLocked() && $op != 'clone',
    // warning, does this method exist?
    '#machine_name' => array(
      'exists' => 'bctypes',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this entity type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => t(isset($entity_type->description) ? $entity_type->description : ''),
    '#description' => t('Description about the entity type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save entity type'),
    '#weight' => 40,
  );

  if (!$entity_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete entity type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('bctype_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Submit handler for creating/editing entity_type.
 * @param $form
 * @param $form_state
 */
function bctype_form_submit(&$form, &$form_state) {
  $entity_type = entity_ui_form_submit_build_entity($form, $form_state);
// Save and go back.
  bctype_save($entity_type);

// Redirect user back to list of entity types.
  $form_state['redirect'] = 'admin/structure/beancount';
}

function bctype_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/beancount/' . $form_state['bctype']->type . '/delete';
}

/**
 * Entity Type delete form.
 * @param $form
 * @param $form_state
 * @param $entity_type
 * @return mixed
 */
function bctype_form_delete_confirm($form, &$form_state, $entity_type) {
  $form_state['entity_type'] = $entity_type;
// Always provide entity id in the same form key as in the entity edit form.
  $form['entity_type_id'] = array(
    '#type' => 'value',
    '#value' => entity_id('bctype', $entity_type)
  );
  return confirm_form($form,
    t('Are you sure you want to delete entity type %title?', array('%title' => entity_label('entity_type', $entity_type))),
    'entity/' . entity_id('bctype', $entity_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Entity Type delete form submit handler.
 * @param $form
 * @param $form_state
 */
function bctype_form_delete_confirm_submit($form, &$form_state) {
  $entity_type = $form_state['entity_type'];
  bctype_delete($entity_type);

  watchdog('bctype', '@type: deleted %title.', array(
    '@type' => $entity_type->type,
    '%title' => $entity_type->label
  ));
  drupal_set_message(t('@type %title has been deleted.', array(
    '@type' => $entity_type->type,
    '%title' => $entity_type->label
  )));

  $form_state['redirect'] = 'admin/structure/beancount';
}

