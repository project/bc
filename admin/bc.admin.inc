<?php

module_load_include('inc', 'bc', 'bc');

/**
 * Page to select Entity Type to add new entity.
 */
function bc_landingpage() {
  // $bookid = _bc_user_default_bookid();

  $form = array();
  foreach (bctypes() as $bundle => $entity) {
    $form[$bundle] = array(
        '#type' => 'fieldset',
        '#title' => t(ucfirst($bundle)),
        '#attached' => array(
          'library' => array(
            array('system', 'drupal.collapse'),
          ),
        ),
        '#attributes' => array(
          'class' => array(
            'collapsible', 'collapsed' ), ),
      );
    if( $subtypes = _bc_list_subtype($bundle) ) {
      $header = array(
        'type' => t($entity->label() . ' type'),
        'operations' => t('Operations'),
      );
      $options = array();
      $operations = array();
      foreach ( $subtypes as $subtype) {
        $base_path = 'beancount/' . $bundle . '/' . $subtype;
        $options[$subtype] = array(
          'type' => array(
            'data' => array(
              '#theme' => 'links',
              '#links' => array(
                $subtype => array(
                  'title' => ucfirst(t($subtype)),
                  'href' => $base_path
                ),
              ),
              '#attributes' => array('class' => array('links', 'inline')),
            ),
          ),
          'operations' => array(
            'data' => array(
              '#theme' => 'links',
              '#links' => array(
                'add' => array(
                  'title' => t('New'),
                  'href' => $base_path . '/add',
                ),
              ),
              '#attributes' => array('class' => array('links', 'inline')),
            ),
          ),
        );
      }
      $form[$bundle]['manage'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $options,
        '#empty' => t('None created yet.'),
      );
    }
    else {
      $base_path = 'beancount/' . $bundle;
      $operations['view'] = array(
        'title' => t('View'),
        'href' => $base_path,
        // 'query' => $destination,
      );
      $operations['add'] = array(
        'title' => t('New'),
        'href' => $base_path . '/add',
        // 'query' => $destination,
      );
      $form[$bundle]['operations'] = array(
        'data' => array(
          '#theme' => 'links',
          '#links' => $operations,
          '#attributes' => array('class' => array('links', 'inline')),
        ),
      );

    }
  }
  return $form;
}


/**
 * Add new entity page callback.
 * @param $type
 * @return array|mixed
 * @groups callbacks
 */
function bc_add($type) {
  $entity_type = bctypes($type);

  $entity = entity_create('bc', array('type' => $type));
  $entity->bookid = _bc_user_default_bookid();
  drupal_set_title(t('Create @name', array('@name' => entity_label('entity_type', $entity_type))));
  $output = drupal_get_form('bc_form', $entity);

  return $output;
}

/**
 * Implements default callback for ctools plugin bc:entity 'form' function
 * Add new entity page callback.
 * @param $type
 * @return array|mixed
 * @groups callbacks
 */
function _bc_form_default($form, &$form_state, $entity) {
  $form_state['entity'] = $entity;

  if (!($bookid = _bc_user_default_bookid()) || !user_access('administer beancount entities')) {
    drupal_set_message(t('You have no books. See your site administrator.'));
    return (NULL);
  }

  // TODO: this should get converted to a db query for books accessible to user, then build select list, and fill with default if only one and make field non-editable
  $form['bookid'] = array(
    '#type' => 'textfield',
    '#title' => t('Book ID'),
    '#access' => FALSE,
    '#default_value' => empty($entity->bookid) ? $bookid : $entity->bookid,
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name'),
    '#default_value' => empty($entity->name) ? '' : t($entity->name),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => empty($entity->description) ? '' : t($entity->description),
  );

  $form['isactive'] = array(
    '#type' => 'checkbox',
    '#title' => 'Active',
    '#default_value' => empty($entity->isactive) ? FALSE : TRUE,
    '#description' => t('Status: active / disabled of the entity type.'),
  );

  // Allow select list for the uid for book entries
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $entity->uid,
  );

  // TODO: parent field will need to be added as a select list of some sort w. db query based on bookid and bundle
  field_attach_form('bc', $entity, $form, $form_state);

  // conditionally load fields or manipulate form per bundle
  foreach( _bc_ctools_entity_info() as $key => $info) {
    $function = 'form alter ' . $entity->type;

    if($function = ctools_plugin_get_function($info, $function)) {
      $function( $form, $entity );
    }
  }

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save ' . $entity->typeLabel()),
    '#submit' => $submit + array('bc_form_submit'),
  );

  // Show Delete button if we edit entity.
  $entity_id = entity_id('bc', $entity);
  if (!empty($entity_id) && bc_access('edit', $entity)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('bc_form_submit_delete'),
    );
  }

  // example of how to set importterm mapper in form. defaults in qbxml are fine
  // $form[QBXML_FORM_REQUEST_PREFIX . 'callback_importtermmapper'] = array(
  //  '#type' => 'textfield',
  //  '#access' => FALSE,
  //  '#value' => 'bc_importtermmapper' );

  if (isset($form['bc_type']) && empty($form_state['entity']->is_new)) {
    $form['bc_type']['#disabled'] = TRUE;
  }

  return $form;
}

/**
 * Entity Form.
 * TODO: Work out fine-grained resolution for accessibility to fields based on user and permissions
 */
function bc_form($form, &$form_state, $entity) {
  // allow BC bundle to define its own 'form' function and use Beancount default otherwise
  if( !($function = ctools_plugin_get_function(_bc_ctools_entity_info($entity->type), 'form')) ) {
    $function = '_bc_form_default';
  }
  return $function($form, $form_state, $entity);
}

/**
 * If qbxml sending will happen, then qbxml module must exist
 * @param $form
 * @param $form_state
 */
function bc_form_validate($form, &$form_state) {
  // TODO: _bc_check_duplicate_name($form, $form_state); // check for dupe name and modify if so. non-fatal
  if (!empty($form_state['values']['bc_type']['und'][0]['tid'])) {
    $tid = $form_state['values']['bc_type']['und'][0]['tid'];
    $subterm = taxonomy_term_load($tid);
    $form_state['entity']->subtype = _bc_list_subtype($form['#bundle'], $subterm->name);
  }
  // letting all entity plugins define validation callbacks even for other entities
  // if this gets too time-consuming or is not used, then it should be changed to
  // just validating with the defining entity plugin
  foreach( _bc_ctools_entity_info() as $name => $info ){
    $function = 'form validate ' . $form['#bundle'];
    if( $function = ctools_plugin_get_function($info, $function) ) {
      $function( $form, $form_state );
    }
  }
}

/**
 * Extract the ListID from the BookID::ListID field
 * @param $extid
 * @return string
 */
function _bc_filterextid($extid) {
  return ($listID = stristr($extid, '::'))
    ? substr($listID, 2)
    : $extid;
}

/**
 * Get the list of currently available roles for this bookid (user bookid as default)
 * @param int $bookid
 * @return array
 */
function _bc_get_roles($bookid = 0) {
  if (!$bookid) {
    $bookid = _bc_user_default_bookid();
    if (!$bookid) {
      return (array());
    }
  }
  $query = db_select('bc', 'bc')
    ->condition('bc.bookid', $bookid)
    ->condition('bc.type', 'role')
    ->fields('bc', array('name'))
    ->execute();
  return ($query->fetchCol());
}

/**
 * Implements ctools plugin bc:entity callback 'addmod external sync'
 * Generate an add or mod request and send to QBXML if flag is set and qbxml module is enabled
 * This function is squarely in the gray area where bc becomes dependent on qbxml
 * @param $entity
 * @param $form_state
 * @group callbacks
 */
function bc_qbxml_form_generate_addmod($entity, &$form_state, array $changedfields = NULL) {
  // if bc already exists in QB, then generate a modify request
  // find the book entity and check whether it syncs (editsequence)
  if (!_bc_book_syncs($entity->bookid)) {
    return;
  }
  $qbcmd_base = ucfirst($entity->typeLabel());
  $qbcmd_alttheme = $qbcmd_base;
  if ($qbcmd_base != 'Account') {
    $qbcmd_base = ucfirst(_bc_term_from_tid($entity->bc_type));
  }
  if ($entity->editsequence) {  // entity exists, prepare mod request
    $qbcmd_opcode = 'Mod';
    if ($changedfields) {
      // always make ListID/extid and EditSequence/editsequence available to theme
      $values = array(
        'extid' => _bc_filterextid($entity->extid),
        // 'editsequence' => $entity->editsequence
        'bcid' => $entity->bcid
        // went away from editsequence, to looking up edit sequence with callback when generating the QBXML
      );
      foreach ($changedfields as $dirtyfield) {
        $values[$dirtyfield] = $form_state['values'][$dirtyfield];
      }
    }
  }
  else { // generate add request
    $qbcmd_opcode = 'Add';
    $values = $form_state['values'];
  }
  $request = qbxml_form_generate_request($values, $qbcmd_base, $qbcmd_alttheme, $qbcmd_opcode);

  $qbxmlmsgsrq = qbxml_generate_qbxmlmsgsrq($request);
  qbxml_action_add_xml_array_to_out_queue($form_state['values']['bookid'], $qbxmlmsgsrq);
}

/**
 * Generate data array from specially named form value
 * @param $data
 * @param $values
 */
function _bc_data_extract(&$data, $values, bool $setemptyvalues) {
  foreach (preg_grep('/^-data-/', array_keys($values)) as $key) {
    $array_keys = explode('-', $key);
    array_shift($array_keys); // shift off initial empty

    if (isset($values[$key])) {
      $value = $values[$key];
    }
    if (isset($values['und'])) {
      $value = $values['und'];
    }

    if (isset($value)) {
      if ($value || $setemptyvalues) {
        qbxml_set_deep_array_element($data, $array_keys, $value);
      }
    }
    unset($value);
  }
}

/**
 * Collect extra data as an arbitrary data array from anything named -data-*
 * @param $form_state
 * @return array

function _bc_form_get_data( $form_state, bool $setemptyvalues=TRUE ){
 * $data = array();
 * _bc_data_extract( $data, $form_state['values'], $setemptyvalues );
 * foreach( $form_state['values'] as $key => $value ){
 * if( is_array($value) ) {
 * _bc_data_extract( $data, $value, $setemptyvalues );
 * }
 * }
 * return( $data );
 * }
 */

/*
 * get flattened keys of recursive difference of two arrays
 */
function _bc_diff_data_arrays_return_flattened_keys(&$flatres, $prefix, $arr1, $arr2) {
  if (empty($arr1)) {
    $arr1 = array();
  }
  if (empty($arr2)) {
    $arr2 = array();
  }
  foreach (array_keys($arr1 + $arr2) as $key) {
    if (isset($arr1[$key])) {
      if (is_array($arr1[$key])) {
        _bc_diff_data_arrays_return_flattened_keys($flatres, $prefix . '-' . $key, $arr1[$key], isset($arr2[$key]) ? $arr2[$key] : NULL);
      }
      else {
        if ((!isset($arr2[$key])) || $arr1[$key] != $arr2[$key]) {
          $flatres[] = $prefix . '-' . $key;
        }
      }
    }
    else {
      if (is_array($arr2[$key])) {
        _bc_diff_data_arrays_return_flattened_keys($flatres, $prefix . '-' . $key, NULL, $arr2[$key]);
      }
      else {
        $flatres[] = $prefix . '-' . $key;
      }
    }
  }
}

/**
 * Make a list of all the fields that have been changed. We'll only update those
 * @param $orig
 * @param $new
 * @return array
 */
function _bc_changedproperties($orig, $new) {
  $ret = array();
  foreach ($orig as $key => &$value) {
    if ($value != $new->$key) {
      if ($key == 'data') {
        $arrdifflat = array();
        _bc_diff_data_arrays_return_flattened_keys($arrdifflat, '', json_decode($value, TRUE), json_decode($new->data, TRUE));
        if ($arrdifflat) {
          $ret = array_merge($ret, $arrdifflat);
        }
      }
      else {
        _bc_trim_empty_arrays($new->$key);
        if ($value != $new->$key) {
          $ret[] = $key;
        }
      }
    }
  }
  return ($ret);
}

/**
 * Entity submit handler.
 * @param $form
 * @param $form_state
 */
function bc_form_submit($form, &$form_state) {
  $entity = $form_state['entity'];
  if (empty($entity->is_new)) {
    $oldentity = clone($entity);
  }
  entity_form_submit_build_entity('bc', $entity, $form, $form_state);
  if ($data = array('data' => _bc_unflatten_array($form_state['values']))) {
    $entity->data = json_encode($data);
  }
  // compare old copy to new
  if (!empty($entity->is_new) || ($changedproperties = _bc_changedproperties($oldentity, $entity))) {
    bc_save($entity);
    if( $addmodsynccallback = ctools_plugin_get_function(_bc_ctools_entity_info($entity->type), 'addmod external sync') ) {
      $addmodsynccallback($entity, $form_state, isset($changedproperties) ? $changedproperties : NULL);
    }
    drupal_set_message(t('Beancount %type %title saved.', array(
      '%type' => t($entity->typelabel()),
      '%title' => entity_label('bc', $entity)
    )));
  }
  $entity_uri = entity_uri('bc', $entity);
  $form_state['redirect'] = $entity_uri['path'];
}

/**
 * Entity delete handler
 * @param $form
 * @param $form_state
 */
function bc_form_submit_delete($form, &$form_state) {
  $entity = $form_state['entity'];
  $entity_uri = entity_uri('bc', $entity);
  $form_state['redirect'] = $entity_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function bc_delete_form($form, &$form_state, $entity) {
  $form_state['entity'] = $entity;
  // Always provide entity id in the same form key as in the entity edit form.
  // todo: if this is a Quickbooks entity, then it might not be deletable, but rather inactivated
  // todo: also if other things depend on this entity, then that needs to be checked before deleting it
  $form['entity_type_id'] = array(
    '#type' => 'value',
    '#value' => entity_id('bc', $entity)
  );
  $entity_uri = entity_uri('bc', $entity);
  return confirm_form($form,
    t('Are you sure you want to delete entity %title?', array('%title' => entity_label('bc', $entity))),
    $entity_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 * @param $form
 * @param $form_state
 */
function bc_delete_form_submit($form, &$form_state) {
  $entity = $form_state['entity'];
  bc_delete($entity);

  drupal_set_message(t('Beancount entity %title deleted.', array('%title' => entity_label('bc', $entity))));

  $form_state['redirect'] = '<front>';
}
